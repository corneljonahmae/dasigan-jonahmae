console.log("JS Objects!");

//[Objects]
/*
	-An object is a data type that is used to represent real world objects
	iInformation stored in objects are represented in a "key:value" pair
	-A key is mostly referred to as a "property" of an object
	-different data types may also be stored in an object's property creating complex data structures

*/

/*
	Syntax 
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}

*/

let ninja = {
	name : "Naruto",
	village: "Konoha",
	occupation: "Hokage"
}

console.log("Result from creating objects using initializers/ literals notation");
console.log(ninja);
console.log(typeof ninja);

let dog = {
	name: "Whitey",
	color: "white",
	breed: "Chihuahua"
}

//Create objects using constructor function
//create a reusable function to create several objects that have the same data structure
//Instance is a concrete occurence of any object which emphasizes on its distinct/unique identity of it
/*
	function ObjectName(keyA,keyB){
		this.keyA = keyA;
		this.keyB - keyB;
	}

*/
	function Laptop(name,manufactureDate){
		this.name = name;
		this.manufactureDate = manufactureDate;
	}

	/*
	"this" keyword allows us to assign new object's properties by associating them
	with the values received from a constructor function's parameter
	*/

	//Instances

	/*
		"new" operator creates an INSTANCE of an object
		Object and instances are often interchanged because of object
		literals (let object = {}) and instances (let object = new Object)
		are distinct/unique objects
	*/

	let laptop1 = new Laptop('Lenovo',2022);
	console.log('Result from creating objects using object constructor');
	console.log(laptop1);

	let myLaptop = new Laptop('MacBook Air',2020);
	console.log('Result from creating objects using object constructor');
	console.log(myLaptop);

	/* invoke/call "Laptop" instead of creating a new object instance
	returns undefined without the "new" operator because the "Laptop" function
	does not have a return statement 
	*/

	//let oldLaptop = Laptop('Portal R2E CCMC',1908);
	// console.log('Result from creating instance without new keyword');
	// console.log(oldLaptop);

	//Mini Activity 2

	let laptop3 = new Laptop('MacBook Pro',2022);
	console.log('Result from creating objects using object constructor');
	console.log(laptop3);

	let laptop4 = new Laptop('Acer',2020);
	console.log('Result from creating objects using object constructor');
	console.log(laptop4);

	let laptop5 = new Laptop('Dell',2020);
	console.log('Result from creating objects using object constructor');
	console.log(laptop5);


//Create empty objects

	let computer = {};
	let myComputer = new Object ();
	console.log(computer);
	console.log(myComputer);


//[Access Object Properties]

	//1. dot notation
	console.log('Result of dot notation: ' + myLaptop.name);
	console.log('Result of dot notation: ' + myLaptop.manufactureDate);

	//2.square bracket notation
	console.log('Result square bracket notation: ' + myLaptop['name']);
	console.log('Result square bracket notation: ' + myLaptop['manufactureDate']);

//Access array objects
/*
	Accessing array elements can also be done using square brackets
	Acesssing object propertyies using the square bracket notation, and array insdexes
	can cause confusion
*/

	let array = [laptop1, myLaptop];

	//square bracket
	console.log(array[0]['name']);

	//dot notation
	console.log(array[0].name);


	//[initialize, add , delete, reassign object properties]

	let car = {};

	car.name = 'Honda Civic';
	console.log("result form adding properties using dot notation:")
	console.log(car);

	// car.number = [1,2,3];
	// console.log(car);

	car['manufacture date'] = 2019;
	console.log(car['manufacture date']);
	//console.log(car.manufacture date)
	console.log(car);

	console.log(car.manufactureDate);

	//delete object properties
	delete car['manufacture date'];
	console.log('Result from deleting properties:')
	console.log(car);

	//reassign object properties
	car.name = 'Dodge Charger R/T';
	console.log('Result from reassigning properties:')
	console.log(car);

//[object methods]

/*
	A method is a function which is a property of an object
	Similar functions/ features of real worrld objects, methods are defined   based on
	what an object is capable of doing and hpw it should work
*/

let person = {
	name : "Cardo",
	talk: function(){
		console.log('Hello my name is ' + this.name );
	}
}

console.log(person);
console.log('Result of object methods:');
person.talk();

person.walk = function(){
	console.log(this.name + ' walked 25 steps forward!')
}
person.walk();

let friend = {
	firstName: 'Nami',
	lastName: 'Misko',
	address: {
		city: 'Tokyo',
		country: 'Japan'
	},
	emails: ['nami@sea.com','namimisko@gmail.com'],
	introduce: function(){
		console.log('Hello! My name is ' + this.firstName + ' ' + this.lastName);
	}
}
friend.introduce();

//Real World Application of Objects!]

//Use object literals

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This Pokemon tackled Target Pokemon!")
		console.log("Target Pokemon's health is now reduced to Target Pokemon Health!")
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}
console.log(myPokemon);

//create an object constructor

function Pokemon(name,level) {

	//Porperties

	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	//methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to _targetPokemon")
	}
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}

}

let pikachu = new Pokemon("Pikachu",16);
let rattata = new Pokemon("Rattata",8);
pikachu.tackle(rattata);
rattata.faint();