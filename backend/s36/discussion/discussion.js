//[MongoDB Aggregation]
/*
	Going to join data from multiple documents
	Aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a Frontend application
*/

//db - market_db
//collection - fruits

//in mongosh, you can create a db. simply use - use <db>
//in mongosh, you can create a collection. You can simply use - db.createCollection (<collectionName>)

	//create a market_db database and fruits collection
	//Create documents to use for the discussion

	db.fruits.insertMany([

		{
			name: "Apple",
			color: "Red",
			stock: 20,
			price: 40,
			supplier_id: 1,
			onSale: true,
			origin: ["Philippines", "US"]
		},
		{
			name: "Banana",
			color: "Yellow",
			stock: 15,
			price: 20,
			supplier_id: 2,
			onSale: true,
			origin: ["Philippines", "Ecuador"]
		},
		{
			name: "Kiwi",
			color: "Green",
			stock: 25,
			price: 50,
			supplier_id: 1,
			onSale: true,
			origin: ["US", "China"]
		},
		{
			name: "Mango",
			color: "Yellow",
			stock: 10,
			price: 120,
			supplier_id: 2,
			onSale: false,
			origin: ["Philippines", "India"]
		}
	])
	//use the aggregate method
	/*
		Syntax:
		db.collectionName.aggregate([
			{ $match: {fieldA: value} },
			{ $group: {_id: "fieldB"}, result: {operation} }
		])

		the $match is used to pass the documents that meet the specified condition(s) to the next pipeline stage/ aggregation process
			Syntax:
				{ $match: {field: value} }

		the $groupis used to group elemenmts together and field-value pairs using the data from the grouped elements
			Syntax:
				{$group: { id: "value", fieldresult: "valueResult"}}

		*/

		//Using both $match and $group along with aggregation will find for fruits that are onSale and will group the total amount of stocks per supplier

		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
			
		])

		//_id, total 45
		//_id, total 15

		//field projection with aggregation
		/*
			the $project can be used when we are aggregating data to include/exclude fields from the returned results
			Syntax: 
				{$project: {field:1/0}}
		*/

		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$project: {_id:0} }
		])

		//total 15
		//total 45

		//Sorting aggregated results
		/*
			the $sort can be used to change the order of aggregated results
			providing the value of -1, we will sort the aggregated results in reverse order
				Syntax:
					{ $sort : {field: 1/ -1}}
				//1
				//-1

		*/


			db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$sort: {total: -1}}
		])
		//total 45 (descending)
		//total 15


			db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$sort: {total: 1}}
		])
		//total 15 (ascending)
		//total 45

		//Aggregating values based on array fields
		/*
			the $unwind deconstructs an array field from a collection/ field with an array value to output a result for each element
			Syntax:
			{$unwind: field}
		*/

		db.fruits.aggregate([
				{$unwind: "$origin"}
			])		
		//we have 8 now 8 separate results based on array elements

		//$sum calculates and gives the collective sum of number values
		//the 1 passed in $sum is used to increment the count for each group by 1

		db.fruits.aggregate([
				{$unwind: "$origin"},
				{$group: {_id: "$origin", kinds: { $sum: 1} } }
			])	

		//MA

		db.fruits.aggregate([
			{$match: {color: "Yellow"}},
			{$count: "Yellow"}
		])


		db.fruits.aggregate([
			{$match: {color: {$regex: 'yELLow', $options:'i'}}},
			{$count: "Yellow"}
		])


		db.fruits.aggregate([
			{$match: {stock: {$lte:10}}},
			{$count: "stock"}
		])

		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$max: "$stock"}}},
		])
