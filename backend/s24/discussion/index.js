console.log("JavaScript Loops!")

//Mini Activity

function greeting() {
	console.log("You are amazing!")
};

let countNum = 50;

while (countNum !==0) {
	console.log("This is printed inside the sample loop:" + countNum);
	greeting()
	countNum--;
}

//[While loop]
//takes in an expression/condition
//expressions are any unit of code that can be evalueated to a value
//if the condition evaluates to true, the statements inside the code block will be executed
//a loop will iterate to a certain number of times until an expression or condition is met
//iteration - is the term given to the repition of statements




/*
	Syntax:
	while (expression/conditon) {
	
	}

*/ 

let count = 5;

//while the value of count is not equal to 0
while(count !== 0){

	//the current value of count is printed out
	console.log("While: " + count);

	//decreases the value of count by 1 every iteration to stop the loop when it reaches 0
	count--;
}

//[Do while loop]
/*
	-this works a lot like while loop
	-but unlik the while loops, do-while loops guarantee that code will be executed at least once

	Syntax
	do {
		statement 
	}while(expression/condition)
*/

let number = Number(prompt("Give me a number: "));

do {
	console.log("Do while:" + number);

	//increase the value of number 1 after every iteration to stop the loop when it reaches 10 or greater
	//number = number + 1
	number += 1;


}while(number < 10)

//[for loop]

/*
	A for loop is more flexible than while and do-while loop
	It consists of three parts:
	1. Initialization - value that will track the progression of the loop
	2. Expression/ Condition - that will be evaluated which will determine whether the loop will run one more time
	3. finalExpression- indicates how to advance the loop 

	Syntax

	for (initialization;expression/condition;finalExpression){
	statement
	}

*/

/*
-Will create a loop that will start from 0 and end at 20
-Every iteration of the loop, the value of count will be checked if it is equal or less than 20
-if the value of count is less than or equal to 20, the statament inside the loop will execute
-the value of count will be incremented by one for each iteration
*/

for (let count = 0; count <= 20; count++){
	console.log("For: " + count);
}

//[Strings]

let myString = "Taylor Swift"
//characters in strings my be counted using the length property
console.log(myString.length);

//accessing elements of a string
console.log(myString[0]);//T
console.log(myString[1]);//a
console.log(myString[2]);//y

//will create a loop that will print out the individual letters of the myString variable

for(let x = 0; x < myString.length; x++) {
	console.log(myString[x])
}

//create a string named "myName" with a value of your name

let myName = "Cardo";


/*
	
*/
for (let i=0; i<myName.length; i++){

	//console.log(myName[i].toLowerCase());

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" ){

		console.log(3);
	}

	else{
		console.log(myName[i]);
	}

	
}

//[Continue and break stataments]

/*
	-continue statement allows the code to go to the next iteration of the lopp without finishing the execution of all elements in a code block
	-break statament is used to terminate the current loop once a match has been found	
*/


/*
	create a loop that if the count value is divisible by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
*/

for (let count = 0; count <= 20; count++){

	if(count % 2 === 0){
		continue; 
	}

	console.log("Continue and Break: " + count);

	if(count>10){
		break;
	}

}

let name = "Bernardo";

for(let i = 0; i < name.length; i++) {
	console.log(name[i])
}

if(name[i].toLowerCase() == "a"){

	console.log("Continue to the next iteration");
	continue;
}
	console.log(name[i]);


if (name[i] == "d");{
	break;
}
