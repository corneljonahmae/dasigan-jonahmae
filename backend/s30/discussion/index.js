console.log("ES6 UPDATES!");

//es6 updates
	//ES6 is one of the latest versions of writing JS and in fact one of the major updates

	//let and const
		//are ES6 updates, these are the new standards of creating variables

	//In JS, hoisting allows us to use functions and variables before they are declared
	//but this may cause confusion, because of the confusion that var hoisting can create, it is best to avoid using variables before they are even declared 

	console.log(varSample);
	var varSample = "Hoist me up!!";

	//if you ahve used nameVar in other parts of the code, you might be surprised at the output might get

	var nameVar = "Jonah"

	if(true){
		var nameVar = "Hi!"
	}
	var nameVar = "C";

	console.log(nameVar);

	let name1 = "Cee";

	if(true){
		let name1 = "Hello!";
	}

	//let name1 = "Hello!";//error bec name1 is already declared

	console.log(name1);

//[** Exponent Operator]
	//Update

	const firstNum = 8**2;
	console.log(firstNum);

	const secondNum = Math.pow(8,2);
	console.log(secondNum);

	let string1 = 'fun';
	let string2 = 'Bootcamp';
	let string3 = 'Coding';
	let string4 = 'JavaScript';
	let string5 = 'Zuitt';
	let string6 = 'love';
	let string7 = 'Learning';
	let string8 = 'I';
	let string9 = 'is';
	let string10 = 'in';


//	MA





//[Template literals `${}`]
/*
	Allows us to write strings without using the concatenation operator (+) 
	greatly healps with readbility
*/
let concatSentence1 = string8 + ' ' + string6 + ' ' + string7 + ' ' + string10 + ' ' + string5 + "." + ' '  + string3 + ' ' + string9 + ' ' + string1 + "."
console.log(concatSentence1);

let concatSentence2 = `${string8} ${string6} ${string5} ${string3} ${string2}!`
console.log(concatSentence2);

/*
	concat method for strings
	let sentence = concatSentence.concat(" .",concatSentence2);
	console.log(sentence);
*/
//${} is a placeholder that is usedw to embed javascirpt expression when creating strings using template literals
let name = "Carding";

let message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

//mutli-line using template literals
const anotherMessage = `

	${name} attended a math competition.

	He won it by solving the problem 8**2 with the solution of ${firstNum}!
`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`)

let dev = {
	name: "Peter",
	lastName: "Parker",
	occupation: "Web developer",
	income: 50000,
	expenses: 60000
};

console.log(`${dev.name} is a ${dev.occupation}.`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}`);

// [Array destructuring]
/*
	Allows us to unpack elements in arrays into distinct variables
	allows us to name array elements with variables instead of using index numbers
	helps with readability

	Syntax
	let/const [variableName, variableName, variableName] = array;

*/

let fullName = ["Juan","Dela","Cruz"];
//pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)

//Array Destructuring

const [firstName,middleName,lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`)

console.log(fullName);

let fruits = ["Mango","Grapes","Guava","Apple"];
let [fruit1, ,fruit3, ] = fruits;
console.log(fruit1);
console.log(fruit3);

let kupunanNiEugene = ["Eugene","Alfred","Vincent","Dennis","Taguro","Master Jeremiah"];
let [kupunan0, kupunan1, kupunan2, kupunan3, , kupunan5] = kupunanNiEugene;
console.log(kupunan0);
console.log(kupunan1);
console.log(kupunan2);
console.log(kupunan3);
console.log(kupunan5);
//[Obecjt destructuring]
/*
	Allows us to unpack properties of objects iunto different variables
	shortens the syntax for accessing properties from objects
	Syntax: 
		let/const {propertyName, propertyName, propertyName} = object;

*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`)

//Object Destructuring
const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`)

function getFullName ({givenName,maidenName,familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person);

//[arrow functions]

/*
	compact alternative syntax to traditional functions
	useful for code snippets where creating functions will not be reused in any other portion of the code
	adhere to "DRY" (Don't Repeat Yourself) principle where there's no longer need to created a fuction and think of a name for functions that will only be uised in  certain code snippets
*/

function displayMsg(){
	console.log("Hi!");
}

displayMsg();

let displayHello = () => {
	console.log("Hello World!")
}

displayHello();

/*function printFullName (firstName,middleInitial,lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John","D","Smith");*/

let printFullName = (firstName,middleInitial,lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullName("John","D","Smith");

const students = ["John","Jane","Natalia","Jobert","Joe"];

//arrow functions with loops

//pre-arrow
students.forEach(function(student){
	console.log(`${student} is a student!`)
})

//arrow
students.forEach((student)=>{
	console.log(`${student} is a student! (from arrow)`)
})

//[Implicit Return Statement]
/*
	There are instances when you can omit the "return" statement
	This works because even without the "return" statement JS implicitly adds it for the reuslt of te function
*/

/*function add (x,y){
	return x + y;
}

let total = add(1,2);
console.log(total);*/

/*const add = (x,y) =>{
	return x + y
}

let total = add(1,2);
console.log(total)*/

const add = (x,y) => x + y

let total = add(1,2);
console.log(total)

//[Default function argument value]

	const greet = (name = 'User') =>{
		return `Good Morning, ${name}!`
	}

	console.log(greet());
	console.log(greet("John"));

//[Class-based object blueprints]
/*
	Allows creation/ instantiation of objects using classes as blueprints
*/






class Car {
	constructor(brand,name,year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


	let myCar = new Car();

	console.log(myCar);

	myCar.brand = "Ford";
	myCar.name = "Ranger Raptor";
	myCar.year = 2021;

	console.log(myCar);

	const myNewCar = new Car("Toyota","Vios",2021);
	console.log(myNewCar);

	//Traditional Functions vs arrow function as methods

let character1 = {

	name: "Cloud Strife",
	occupation: "Soldier",
	greet: ()=> {

		//in a traditional function:
			//this keyword refers to the current object where the method is
		console.log(this);
		console.log(`Hi! I'm ${this.name}`);
	},
	introduceJob: function(){
		console.log(`Hi! I'm ${this.name}. I'm a ${this.occupation}`)
	}
}

character1.greet();
character1.introduceJob();

class character{
	constructor(name,role,strength,weakness){
		this.name = name;
		this.role = role;
		this.strength = strength;
		this.weakness = weakness;
	}
}

let myCharacter = new character ();

	console.log(myCharacter);

	myCharacter.name = "Cecilion";
	myCharacter.role = "Mage";
	myCharacter.strength = "Magic";
	myCharacter.weakness = "Fighters";

	console.log(myCharacter);
