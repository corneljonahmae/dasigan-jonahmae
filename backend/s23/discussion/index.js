console.log("Hello, B297!");

//[if, else if, and else statements]

let numG = -1;


//if statement
//executes a statements if a specified conditon is true

if (numG < 0) {
	console.log("Hello! The condition in the if statment is true!")
}

let numH = 1;

//else if clause
//execute a statment if the previous condition is false and if the specified condition is true
//the else if clause is optional and can be added to capture additional conditions to change the flow of a program

if (numG > 0){
	console.log("Hello!")
}

else if (numH>0) {
	console.log("This will log if else condition is true and the if condition is true!")
}

//else statement
//executes a statment if all other conditions are false
//the "else" statment is optional and can be added to capture any other result to change the flow of our program

if (numG > 0){
	console.log("I'm enchanted to meet you!")
}

else if (numJ = 0) {
	console.log("It's me, Hi...")
}
else {
	console.log("Hello from the other side!")
}


//[if, else if, and else statments with functions]


let message = 'No message!';
console.log(message);

	function determineTyphoonIntensity(windspeed){

		if(windspeed < 30){
			return 'Not a typhoon yet!'
		}
		else if (windspeed <= 61){
			return 'Tropical Depression detected!'
		}
		else if (windspeed >= 62 && windspeed <=88){
			return 'Tropical Storm detected!'
		}
		else if (windspeed >= 89 && windspeed <=117){
			return 'Severe Tropical Storm detected!'
		}
		else {
			return 'Tropical Storm detected!'
		}

	}

	message = determineTyphoonIntensity(65);
	console.log(message);

	//console.warn is a good way to print warningsin our console that could help us devs act on certain output within our code

	if (message == 'Tropical Storm detected!') {
		console.warn(message);
	}

	//[truthy and falsy]

	/*
		-in JS a "truthy" value is a value that is considered true when encountered in
		Boolean Context
		-values are considered true unless defined false

		-Falsy values/ exceptions for truthy:
		1. false
		2. 0
		3. -0
		4. "" - empty string
		5. null
		6. undefined
		7. NaN

	*/

	//truthy examples

	if (true) {
		console.log('This is truthy!')
	}

	if (1) {
		console.log('This is truthy!')
	}

	if ([]) {
		console.log('This is truthy!')
	}

	//Falsy examples

	if (false) {
		console.log('This will not log in the console!')
	}
	if (0) {
		console.log('This will not log in the console!')
	}
	if (undefined) {
		console.log('This will not log in the console!')
	}

	if("Jonah"){
		console.log("This will log in the console.")
	}

	//[Conditonal (ternary) Operator]
	/*
		takes in three operands:
		1. condition
		2. expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy

		Syntax:
			(condition) ? ifTrue : ifFalse
	*/

	//single statement execution
	let ternaryResult = (1 < 18) ? true : false;
	console.log("Result of the ternary operator: " + ternaryResult);

	//multiplt statement execution

	let name;

	function isOfLegalAge() {
		name = 'John';
		return 'You are of the legal age limit!'
	}
		function isUnderAge() {
		name = "Jane";
		return 'You are under the age limit!'
	}

	let age = parseInt(prompt("What is your age?"));
	console.log(age);

	let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	console.log("Resulf of Ternary operator in functions: " + legalAge + ', ' + name);

	//[switch statement]
		//can be used as an alternative to an if, else if, and else statement where the data to be used in the condition of an EXPECTED input

	/*
		Syntax:
		switch (expression){
			
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
	*/

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

		switch (day) {

			case 'monday':
				console.log("The color of the day is blue!");
				break;
			case 'tuesday':
				console.log("The color of the day is yellow!");
				break;
			case 'wednesday':
				console.log("The color of the day is red!");
				break;
			case 'thursday':
				console.log("The color of the day is pink!");
				break;
			case 'friday':
				console.log("The color of the day is white!");
				break;
			case 'saturday':
				console.log("The color of the day is orange!");
				break;
			case 'sunday':
				console.log("The color of the day is gray!");
				break;

			default:
				console.log("Please input a valid day.");
				break;
		}

//[try-catch-finally statement]
	//try catch statements are commonly used for error handling


	function showIntesityAlert(windspeed) {
		try {
			//attempt to execute a code
			alerat(determineTyphoonIntensity (windspeed));
		}

		catch (error) {

			console.log(typeof error);
			//"error.message" is used to access the information relationg to the error object
			console.warn(error.message);

		}

		finally {
			alert('Intensity updates will show new alert')
		}
	}

	showIntesityAlert(56);

	console.log("Hi!")