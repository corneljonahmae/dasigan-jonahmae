console.log("Hello World");

// Arithmetic Operators
// +,-,*,/,%

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum)

//difference

let difference = x - y;
console.log("Result of subtraction operator: " + difference)

//product

let product = x * y;
console.log("Result of multiplication operator: " + product)

//quotient

let quotient = x / y;
console.log("Result of division operator: " + quotient)

//remainder

let remainder = y % x;
console.log("Result of modulo operator: " + remainder)

let a = 10;
let b = 5;

// let remainder = a % b;
// console.log(remainderA);.//0

// Assignment Operator
// Baisc assignment operator (=)

let assignmentNumber = 8;

// Addition assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator

assignmentNumber -= 2;
console.log("Result of -= : " + assignmentNumber);
assignmentNumber *= 2;
console.log("Result of *= : " + assignmentNumber);
assignmentNumber /= 2;
console.log("Result of /= : " + assignmentNumber);

// multiple operators and parentheses

/*
	1. 3 * 4 = 12
	2. 12/ 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/ 

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let mdas2 = 8 / 2 * (2 + 2);
console.log(mdas2);

/*
	1. 4/5 = 0.8
	2. 2-3 = -1
	3. -1 * = -0.8
	4. 1 + -0.8 = 0.2
*/ 

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);

//Increment and Decrement
//operators that add or subtract values by 1 and re-assigns the value of the variable where the increment/decrement was applied to

let z = 1;

// the value of z is added by a value of ine before returning the value and storing it in the variable "increment"

//increase then re-assign

 let increment = ++z;
 console.log("Result of pre-incement: " + increment);
 console.log("Result of pre-incement: " + z);

 //the value of z is returnet and stored in the variable "increment" then the value of z is increased by one

 //re-assign then increase

 increment = z++;
 console.log("Result of post-incement: " + increment);
 console.log("Result of post-incement: " + z);



//decrement
//pre-decrement - decrease then re-assign
 let decrement = --z;
 console.log("Result of pre-decrement: " + decrement);
 console.log("Result of pre-decrement: " + z);


//post-decrement - re-assign then decrease
 console.log("Result of post-decrement: " + decrement);
 console.log("Result of post-decrement: " + z);

//Type coercion
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another
*/ 

 let numA = '10';
 let numB = 12;

 let coercion = numA + numB;
 console.log(coercion);//1012
 console.log(typeof Coercion);


 let numC = 16;
 let numD = 14;

 let nonCoercion = numC + numD;
 console.log(nonCoercion);
 console.log(typeof nonCoercion);


// the boolean true is associated with the value of 1
 let numE = true + 1;
 console.log(numE);

// the boolean false is associated with the value of 0
 let numF = false + 1;
 console.log(numF);

 // Comparison operators

 let juan = 'juan';

 // Equality operator

 console.log(1 == 1);
 console.log(1 == 2);
 console.log(1 == '1');
 console.log(0 == false);
 console.log('juan' == 'juan');
 console.log('juan' == juan);

 // inequality operator

 console.log(1 != 1);
 console.log(1 != 2);
 console.log(1 != '1');
 console.log(0 != false);
 console.log('juan' != 'juan');
 console.log('juan' != juan);

 // Strict Equality operator (===)

 console.log(1 === 1);
 console.log(1 === 2);
 console.log(1 === '1');
 console.log(0 === false);
 console.log('juan' === 'juan');
 console.log('juan' == juan);

 // Strict inequality operator (!==)

 console.log(1 !== 1);
 console.log(1 !== 2);
 console.log(1 !== '1');
 console.log(0 !== false);
 console.log('juan' !== 'juan');
 console.log('juan' !== juan);

 // Relational Operators

 let abc = 50;
 let def = 65;

 let isGreaterThan = abc > def;
 let isLessThan = abc >= def;
 let isGTOrEqual = abc <= def;
 let isLTOrEqual = abc > def;

 console.log(isGreaterThan);
 console.log(isLessThan);
 console.log(isGTOrEqual);
 console.log(isLTOrEqual);

 let numStr = "30";
 console.log(abc > numStr);//forced coercion to change string to a number
 console.log(def <= numStr);

 let str = "twenty";
 console.log(def >= str);

 //Logical operators

 let isLegalAge = true;
 let isRegistered = false;

 //&& (and), || (or) , ! (not)

 //&&
 //return if all operators are true

 let allRequirementsMet = isLegalAge && isRegistered;
 console.log("Result of AND operator: " + allRequirementsMet);

 //||
//returns true if ONE of the operands are ture;

 let someRequirementsMet = isLegalAge || isRegistered;
 console.log("Result of OR operator: " + someRequirementsMet);

 //!
 //returns the opposite value
let someRequirementsMetnotMet = !isRegistered;
console.log("Result of NOT operator: " + someRequirementsMet);


