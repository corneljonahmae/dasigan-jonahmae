let http = require ("http");

//We use require directive to include and load Node.js modules
	//A module is a software component or part of a program that contains one or more routines
	//Modules are objects that contain pre-built codes, methods and data

//http is a default odule that comes from nodejs. It allows us to transfer data using HTTP and use methods that let us create servers
//http module lets node.js transfer data using hyper text transfer protocol
//http is a protocol that allows the fetching of resources such as HTML documents
	//protocol to cient-server communication
		//http://localhost:4000 - server/application

//What is a client?
//A client is an application whcih creates requests for resources from a server. A client will trigger an action, in the web development context, through a URl and wait for the response of the server.

//What is a server?
//A server is able to host and deliver resources that request by a client

/*
	What is node.js?
	Nodejs is a runtime environment which allows us to create/develop backend/server-side applications with JS. Because by default, JS was conceptualized solely to frontend

	Runtime environment - is the environment in which a program or application is executed


*/


http.createServer(function(request,response){

	/*
		createServer() method is a method from the http module that allows us to handle requests and response from a client and a server respectively

		.createrServer() method takes a function argument which is able to receive 2 objects:
		1. The request object which contains details of the req from the client
		2. The response object which contains details of the res from the server

		The createServer() method ALWAYS receives the receives the request object frist before the response. 

	*/

	response.writeHead(200,{'Content-type':'text/plain'})

	/*
		response.writeHead() is a method of the response object
			-it allows us to add headers to our response
		1. HTTP Status code
			200 means ok
			404 means the resource cannot be found
		2. 'Content-type'
			-pertains to the data type of our response

	*/

	response.end('Hi, my name is JM!')
	/*
		response.end()
			-ends our response
			It is also able to send a message/data as a string

	*/

}).listen(4000)

console.log('Server is running at localhost:4000')


/*
	.listen() allows us to assign a port to our server
	This will allow us to serve our index.js server in our loacl machine assigned to port 4000

	4000,4040,8000,5000,3000,4200 - ususally used for web development

*/