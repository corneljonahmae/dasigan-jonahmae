console.log("Array Non-Mutator and Iterator Methods");

/*
Non-mutator methods
-non-mutaotr methods are functions that do not modify or change an array after
they are created
-do not manipulate the original array performing various tasks such as:
	-returning elements from an array
	-combining arrays
	-printing the output

*/

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

//indexof()
/*
	-returns the index number of the first matching element found in an array
	-if no matches found, the result will be -1
*/

	let firstIndex = countries.indexOf('PH');
	console.log('Result of indexOf method: ' + firstIndex);

	let invalidCountry = countries.indexOf('BR');
	console.log('Result of indexOf method: ' + invalidCountry);

//lastIndexOf
/*
	Returns the index number of the last matching element found in the array
*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log('Result of lastIndexOf method: ' + lastIndex);

	let lastIndexStart = countries.lastIndexOf('PH', 6);//5
	//let lastIndexStart = countries.lastIndexOf('PH', 3);//1
	console.log('Result of lastIndexOf method: ' + lastIndexStart);

//slice()
//-portions or slices elements from an array and returns a new array

console.log(countries);

let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log('Result from slice method: ');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method: ');
console.log(slicedArrayC);

//toString()
//returns an array as a string separated as commas

let stringArray = countries.toString();
console.log('Result from toString method: ');
console.log(stringArray);

let sampArray = [1,2,3];
console.log(sampArray.toString());

//concat
//combines 2 arrays and returns the combined results

let taskArrayA = ['drink html','eat javascipt'];
let taskArrayB = ['inhale css','breathe mongodb'];
let taskArrayC = ['get git','be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from concat method: ');
console.log(tasks);

console.log('Result from concat method: ');
let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);

let combinedTasks = taskArrayA.concat('smell express','throw react');
console.log('Result from concat method: ');
console.log(combinedTasks);

//join
//returns an array as a string separated by specified separator

let users = ['John','Jane','Joe','James'];
console.log(users.join());//default
console.log(users.join(''));
console.log(users.join(' - '));

//iterator methods
//these are loops designed to perform repititive tasks on arrays
//array iterations methods normally work with a functions supplied as an argument

//forEach()
//-similar to a for loop that iterated on each array element
//-for each item in the array, the anonymous function passed in the forEach() method will be running
//-the anonymous function is able to receive the current item being iterated or loop over by assigning a parameter 
//-variable names for arrays are normally written in the plural form
//-it is a common practive to use singular form of the array content for the parameter names used in array loops
//-forEach() does not return anything

console.log(allTasks);


	allTasks.forEach(function(task){
		console.log(task);
	});

	let filteredTasks = [];

	allTasks.forEach(function(task){
	
		if(task.length>10){
			filteredTasks.push(task);

		}

	});

	console.log('Result of filteredTasks:');
	console.log(filteredTasks);

//map()
//-this iterates on each element and returns new array with different values depending on the reuslt of the function's operation
//-we require the use of "return"

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number){
	return number * number;
});

console.log('Original Array:')
console.log(numbers);
console.log('Result of map method: ')
console.log(numberMap);

//map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number * number;
})

console.log(numberForEach);

//every()
//-checks if all elements in an array meet a given condition

let allValid = numbers.every(function(number){
	return (number < 3);
	//return (are all numbers less than 3)
})
console.log('Result of every method:');
console.log(allValid);//false

//some()
//-it checks if at least one element in an array meets the given condition

let someValid = numbers.some(function(number){
	return (number < 2);
	//return are some numbers less than 2
})
console.log('Result of some method:');
console.log(someValid);

//filter
//-returns a new array that contains element which meets the given condition
//-returns an empty array if no elements were found

let filterValid = numbers.filter(function(number){
	return(number < 3);
	//return numbers that are less than 3
})
console.log('Result of filter method:');
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number = 0);
})
console.log('Result of filter method:');
console.log(nothingFound);

//MA
let filteredNumbers = [];

numbers.forEach(function(number){
	if (number < 3){
		filteredNumbers.push(number);
	}
})

console.log(filteredNumbers);

let products = ['Mouse','Keyboard','Laptop','Monitor'];

//includes()
//-checcks if the argument passed can be found in the array


let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

//methods can be chained using them one after another

let filteredProducts = products.filter(function(product){

	return product.toLowerCase().includes('a');
})
console.log(filteredProducts);

//reduce()
//this evaluates element from left to right and returns/ reduces the array into a single value
	//accumulator parameter in the function stores the result for every iteration of the loop
	//current value parameter is the current element in the array that is evaluated in each iteration of the loop

/*
Process:
1. first element in the array is stored in the accumulator
2. second element in the array is stored in the current value
3.an operation is performed on the 2 elements
4. the loop repeats the whole step 1-3 until all elemenets have been worked on

*/

console.log(numbers);//[1,2,3,4,5]
					 //1 + 2 = 3
					 //3 + 3 = 6
					 //6 + 4 = 10
					 //10 + 5 = 15

let iteration = 0;
let reducedArray = numbers.reduce(function(x,y){
	console.warn('Current iteration: ' + ++iteration);
	console.log('Accumulator: '+ x);
	console.log('Current value: ' + y);
	return x + y;
})

console.log("Result of reduce method: " + reducedArray);

console.log(reducedArray);

//reduce string arrays to string 

let list = ['Hello','From','The','Other','Side'];
let reducedJoin = list.reduce(function(x,y){
	console.log(x);
	console.log(y);
	return x + ' ' + y
})
console.log("Result of reduce method: " + reducedJoin);